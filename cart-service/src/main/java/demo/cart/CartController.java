package demo.cart;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import demo.cart.client.CartDTO;
import demo.cart.client.CartService;

@RestController
@RequestMapping("/cart")
public class CartController {

    @Autowired
    private CartService cartService;

    @RequestMapping(value = "/pedidosCliente/{idCliente}/", method = RequestMethod.GET)
    public Collection<CartDTO> getCartsForPayment(@PathVariable("idCliente") Long idCliente) {
        List<CartDTO> products = cartService.getPedidosCliente(idCliente);
        return products;
    }

}