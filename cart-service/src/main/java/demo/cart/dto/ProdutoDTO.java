package demo.cart.dto;

public class ProdutoDTO {

	private Long idProduto;
	private String descricao;
	private String descricaoCompleta;
	private Double precoCusto;
	private Double precoVenda;
	private Boolean vendivel;
	private Double precoComposicao;
	private Long estoque;
	private String imagem;
	
	public Long getIdProduto() {
		return idProduto;
	}

	public void setIdProduto(Long idProduto) {
		this.idProduto = idProduto;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricaoCompleta() {
		return descricaoCompleta;
	}

	public void setDescricaoCompleta(String descricaoCompleta) {
		this.descricaoCompleta = descricaoCompleta;
	}

	public Double getPrecoCusto() {
		return precoCusto;
	}

	public void setPrecoCusto(Double precoCusto) {
		this.precoCusto = precoCusto;
	}

	public Double getPrecoVenda() {
		return precoVenda;
	}

	public void setPrecoVenda(Double precoVenda) {
		this.precoVenda = precoVenda;
	}

	public Boolean getVendivel() {
		return vendivel;
	}

	public void setVendivel(Boolean vendivel) {
		this.vendivel = vendivel;
	}

	public Double getPrecoComposicao() {
		return precoComposicao;
	}

	public void setPrecoComposicao(Double precoComposicao) {
		this.precoComposicao = precoComposicao;
	}

	public Long getEstoque() {
		return estoque;
	}

	public void setEstoque(Long estoque) {
		this.estoque = estoque;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	
	

}