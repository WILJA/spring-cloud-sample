package demo.cart.client;

import java.util.Set;

import demo.cart.dto.ProdutoDTO;

public class CartDTO {

	Long idPedido;

	private Long idCliente;
	private Set<ProdutoDTO> produtos;
	
	public Long getIdPedido() {
		return idPedido;
	}
	public void setIdPedido(Long idPedido) {
		this.idPedido = idPedido;
	}
	public Long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}
	public Set<ProdutoDTO> getProdutos() {
		return produtos;
	}
	public void setProdutos(Set<ProdutoDTO> produtos) {
		this.produtos = produtos;
	}
	
	
}
