package demo.cart.client;

import java.util.List;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.hateoas.PagedResources;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("core-service")
public interface CartService {

    @RequestMapping(method = RequestMethod.GET, path = "/pedido/cliente/{idCliente}")
    PagedResources<CartDTO> getPedidosCliente(@PathVariable("idCliente") Long idCliente, @PageableDefault(page=0, size=10) Pageable pegeable);
    
    @RequestMapping(method = RequestMethod.GET, path = "/pedido/clienteList/{idCliente}")
    List<CartDTO> getPedidosCliente(@PathVariable("idCliente") Long idCliente);

}