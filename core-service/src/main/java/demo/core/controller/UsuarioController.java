package demo.core.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import demo.core.model.Usuario;
import demo.core.repo.UsuarioRepository;
import demo.core.service.UsuarioService;
import demo.core.support.AbstractController;

@RestController
@RequestMapping("/usuario")
public class UsuarioController extends AbstractController<Usuario, UsuarioService> {

    @Autowired
    private UsuarioRepository usuarioRepository;

   
}