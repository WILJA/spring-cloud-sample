package demo.core.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import demo.core.model.Pedido;
import demo.core.repo.PedidoRepository;
import demo.core.service.PedidoService;
import demo.core.support.AbstractController;

@RestController
@RequestMapping("/pedido")
public class PedidoController extends AbstractController<Pedido, PedidoService> {

    @Autowired
    private PedidoRepository pedidoRepository;
    
    @RequestMapping(value = "/cliente/{idCliente}", method = RequestMethod.GET)
    public Page<Pedido> getPedidosCliente(@PathVariable("idCliente") Long idCliente, @PageableDefault(page=0, size=10) Pageable pegeable) {
    	return pedidoRepository.findByIdCliente(idCliente, pegeable);
    }
    
    @RequestMapping(value = "/clienteList/{idCliente}", method = RequestMethod.GET)
    public List<Pedido> getPedidosClientList(@PathVariable("idCliente") Long idCliente) {
    	return pedidoRepository.findByIdCliente(idCliente);
    }

   
}