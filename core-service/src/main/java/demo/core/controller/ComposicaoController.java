package demo.core.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import demo.core.model.Composicao;
import demo.core.repo.ComposicaoRepository;
import demo.core.service.ComposicaoService;
import demo.core.support.AbstractController;

@RestController
@RequestMapping("/composicao")
public class ComposicaoController extends AbstractController<Composicao, ComposicaoService> {

    @Autowired
    private ComposicaoRepository composicaoRepository;

   
}