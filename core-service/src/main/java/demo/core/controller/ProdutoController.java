package demo.core.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import demo.core.model.Produto;
import demo.core.repo.ProdutoRepository;
import demo.core.service.ProdutoService;
import demo.core.support.AbstractController;

@RestController
@RequestMapping("/produto")
public class ProdutoController extends AbstractController<Produto, ProdutoService> {

    @Autowired
    private ProdutoRepository produtoRepository;
    
    @RequestMapping(value = "/all/{name}", method = RequestMethod.GET)
    public Page<Produto> getProdutosByDescricao(@PathVariable("name") String descricao, @PageableDefault(page=0, size=10) Pageable pegeable) {
    	return produtoRepository.findByDescricao(descricao, pegeable);
    }
    
    @RequestMapping(value = "/vendivel/", method = RequestMethod.GET)
    public Page<Produto> getVendiveis(@PageableDefault(page=0, size=10) Pageable pegeable) {
    	return produtoRepository.findByVendivelTrue(pegeable);
    }
   
}