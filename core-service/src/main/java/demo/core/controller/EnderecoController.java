package demo.core.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import demo.core.dto.EnderecoDTO;
import demo.core.model.Endereco;
import demo.core.repo.EnderecoRepository;
import demo.core.service.EnderecoService;
import demo.core.support.AbstractController;

import java.util.Collection;

@RestController
@RequestMapping("/endereco")
public class EnderecoController extends AbstractController<Endereco, EnderecoService> {

    @Autowired
    private EnderecoRepository enderecoRepository;

    @RequestMapping(value = "/enderecos", method = RequestMethod.GET)
    public Collection<Endereco> getEnderecos() {
    	return enderecoRepository.findAll();
    }
    
    @RequestMapping(value = "/{id}/dto", method = RequestMethod.GET)
    public EnderecoDTO getEndereco(@PathVariable Long id) throws Exception {
				
		Endereco endereco = enderecoRepository.findOne(id);

		return EnderecoDTO.builder().idEndereco(endereco.getIdEndereco())
				.logradouro(endereco.getLogradouro())
				.numero(endereco.getNumero())
				.bairro(endereco.getBairro())
				.cep(endereco.getCep())
				.cidade(endereco.getCidade())
				.uf(endereco.getUf())
				.build();
	}

}