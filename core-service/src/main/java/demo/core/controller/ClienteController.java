package demo.core.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import demo.core.model.Cliente;
import demo.core.repo.ClienteRepository;
import demo.core.service.ClienteService;
import demo.core.support.AbstractController;

@RestController
@RequestMapping("/cliente")
public class ClienteController extends AbstractController<Cliente, ClienteService> {

    @Autowired
    private ClienteRepository clienteRepository;

   
}