package demo.core.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import demo.core.model.Composicao;

@RepositoryRestResource
public interface ComposicaoRepository extends JpaRepository<Composicao, Long> {
}
