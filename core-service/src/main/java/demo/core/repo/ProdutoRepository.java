package demo.core.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import demo.core.model.Produto;

@RepositoryRestResource
public interface ProdutoRepository extends JpaRepository<Produto, Long> {
	Page<Produto> findByDescricao(String descricao, Pageable pageable);
	
	Page<Produto> findByVendivelTrue(Pageable pageable);
}
