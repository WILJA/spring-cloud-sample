package demo.core.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import demo.core.model.Pedido;

@RepositoryRestResource
public interface PedidoRepository extends JpaRepository<Pedido, Long> {
	
	Page<Pedido> findByIdCliente(Long id, Pageable pageable);
	
	List<Pedido> findByIdCliente(Long id);
}
