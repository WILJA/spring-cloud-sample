package demo.core.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EnderecoDTO {
	
	 Long idEndereco;
	 String logradouro;
	 Long numero;
	 Long cep;
	 String bairro;
	 String cidade;
	 String uf;
	
}
