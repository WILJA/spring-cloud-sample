package demo.core.model;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import demo.core.support.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "usuario")
public class Usuario extends BaseEntity {

	@Id @GeneratedValue
	@Column(name = "id_usuario")
	Long idUsuario;
	@Column(name = "login")
	String login;
	@Column(name = "senha")
	String senha;
	@JoinColumn(name = "id_cliente", referencedColumnName = "id_cliente")
	@OneToOne(fetch = FetchType.EAGER)
	Cliente cliente;
	@Column(name = "grupo")
	Long grupo;

}