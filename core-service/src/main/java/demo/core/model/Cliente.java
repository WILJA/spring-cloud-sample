package demo.core.model;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import demo.core.support.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "cliente")
public class Cliente extends BaseEntity {


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	@Column(name = "id_cliente")
	 Long idCliente;
	@Column(name = "cpf")
	 Long cpf;
	@Column(name = "nome")
	 String nome;
	@Column(name = "email")
	 String email;
	@JoinColumn(name = "id_endereco", referencedColumnName = "id_endereco")
	@ManyToOne(fetch = FetchType.EAGER)
	 Endereco endereco;

}