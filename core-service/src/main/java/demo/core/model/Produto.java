package demo.core.model;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import demo.core.support.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Table(name = "produto")
public class Produto extends BaseEntity {

	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_produto")
	 Long idProduto;
	@Column(name = "descricao")
	 String descricao;
	@Column(name = "descricao_completa")
	 String descricaoCompleta;
	@Column(name = "preco_custo")
	 Double precoCusto;
	@Column(name = "preco_venda")
	 Double precoVenda;
	@Column(name = "vendivel")
	 Boolean vendivel;
	@Column(name = "preco_composicao")
	 Double precoComposicao;
	@Column(name = "estoque")
	 Long estoque;
	@JoinColumn(name = "id_composicao", referencedColumnName = "id_composicao")
	@ManyToOne(fetch = FetchType.EAGER)
	 Composicao composicao;
	@Column(name = "imagem")
	 String imagem;

}