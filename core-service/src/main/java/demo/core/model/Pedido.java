package demo.core.model;



import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import demo.core.support.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Table(name = "pedido")
public class Pedido extends BaseEntity {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_pedido")
	Long idPedido;
	
	@OneToOne
	Cliente cliente;
	
	@Column(name = "id_cliente")
	private Long idCliente;
	
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="rel_pedido_produtos", joinColumns=
    {@JoinColumn(name="id_pedido")}, inverseJoinColumns=
      {@JoinColumn(name="id_produto")})
	private Set<Produto> produtos;
    
}