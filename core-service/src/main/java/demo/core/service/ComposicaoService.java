package demo.core.service;



import org.springframework.stereotype.Service;

import demo.core.model.Composicao;
import demo.core.repo.ComposicaoRepository;
import demo.core.support.AbstractService;




@Service
public class ComposicaoService extends AbstractService<Composicao, ComposicaoRepository> {
}
