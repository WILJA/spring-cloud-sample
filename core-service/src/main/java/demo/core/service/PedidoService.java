package demo.core.service;



import org.springframework.stereotype.Service;

import demo.core.model.Pedido;
import demo.core.repo.PedidoRepository;
import demo.core.support.AbstractService;




@Service
public class PedidoService extends AbstractService<Pedido, PedidoRepository> {
}
