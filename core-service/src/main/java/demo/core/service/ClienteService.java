package demo.core.service;



import org.springframework.stereotype.Service;

import demo.core.model.Cliente;
import demo.core.repo.ClienteRepository;
import demo.core.support.AbstractService;




@Service
public class ClienteService extends AbstractService<Cliente, ClienteRepository> {
}
