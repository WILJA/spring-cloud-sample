package demo.core.service;



import org.springframework.stereotype.Service;

import demo.core.model.Endereco;
import demo.core.repo.EnderecoRepository;
import demo.core.support.AbstractService;




@Service
public class EnderecoService extends AbstractService<Endereco, EnderecoRepository> {
}
