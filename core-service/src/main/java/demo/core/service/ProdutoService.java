package demo.core.service;



import org.springframework.stereotype.Service;

import demo.core.model.Produto;
import demo.core.repo.ProdutoRepository;
import demo.core.support.AbstractService;




@Service
public class ProdutoService extends AbstractService<Produto, ProdutoRepository> {
}
