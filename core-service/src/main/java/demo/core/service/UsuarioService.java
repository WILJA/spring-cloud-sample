package demo.core.service;



import org.springframework.stereotype.Service;

import demo.core.model.Usuario;
import demo.core.repo.UsuarioRepository;
import demo.core.support.AbstractService;




@Service
public class UsuarioService extends AbstractService<Usuario, UsuarioRepository> {
}
