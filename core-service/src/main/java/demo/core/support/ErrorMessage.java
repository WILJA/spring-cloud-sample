package demo.core.support;

public class ErrorMessage {

    private final Object id;

    private final String message;
    
    public ErrorMessage(Object id, String message) {
        this.id = id;
        this.message = message;
    }
    
    public String getMessage() {
        return message;
    }
    
    public Object getId() {
        return id;
    }

    public void setId(Object id) {
    }
}
