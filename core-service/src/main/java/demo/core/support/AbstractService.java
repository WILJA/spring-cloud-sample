package demo.core.support;

import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public abstract class AbstractService<E extends BaseEntity, R extends JpaRepository<E, Long>> implements BaseService<E> {

	protected static Logger log = LoggerFactory.getLogger(AbstractService.class);

	@Autowired
	protected R repo;
	
	@Override
	public E save(E entity) {
		return repo.save(entity);
	}

	@Override
	public Page<E> list(Pageable pageable) {
		try
		{
			return repo.findAll(pageable);
		}
		catch (ConstraintViolationException e)
		{
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Boolean delete(Long id) {
		repo.delete(id);
		return Boolean.TRUE;
	}

	@Override
	public E get(Long id) {
		try
		{
			E ret = repo.findOne(id);
		
			return ret;
		}
		catch (ConstraintViolationException e)
		{
			e.printStackTrace();
		}
		return null;
		
	}

	public R getRepository() {
		return repo;
	}

}