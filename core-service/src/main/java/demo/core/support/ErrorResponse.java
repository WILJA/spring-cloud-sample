package demo.core.support;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Represents an error response operation
 */
public final class ErrorResponse {

    @JsonProperty
    private final String message;

    public ErrorResponse(String message) {
        this.message = message;
    }    
}
