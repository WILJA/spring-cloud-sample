package demo.core.support;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

public interface BaseController<E extends BaseEntity> {

    @RequestMapping(method = RequestMethod.GET)
    Page<E> all(@PageableDefault(page = 0, size = 10) Pageable pageable);

    @RequestMapping(method = RequestMethod.POST)
    E create(@RequestBody E e);

    @RequestMapping(method= RequestMethod.GET, value = "/{id}")
    Object read(@PathVariable String id);

    @RequestMapping(method = RequestMethod.PUT, value = "/{id}")
    E update(@RequestBody E e, @PathVariable String id);

    @RequestMapping(method= RequestMethod.DELETE, value = "/{id}")
    Object delete(@PathVariable String id);

}
