﻿DROP TABLE IF EXISTS endereco;

CREATE TABLE endereco
(
  id_endereco bigint(20) NOT NULL AUTO_INCREMENT,
  logradouro varchar(255) NOT NULL,
  numero int(11) DEFAULT NULL,
  cep int(11) DEFAULT NULL,
  bairro varchar(255) NOT NULL,
  cidade varchar(255) NOT NULL,
  uf varchar(255) NOT NULL,
  PRIMARY KEY (id_endereco)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO endereco (id_endereco, logradouro, numero, cep, bairro, cidade, uf) VALUES (1, 'Av. Rio Grande', 1351, 88131600, 'Rio Grande', 'PalhoÃ§a', 'SC');
INSERT INTO endereco (id_endereco, logradouro, numero, cep, bairro, cidade, uf) VALUES (2, 'Teste', 1234, 88131600, 'Teste', 'Teste', 'SC');
INSERT INTO endereco (id_endereco, logradouro, numero, cep, bairro, cidade, uf) VALUES (3, 'Teste', 1351, 88131600, 'Rio Grande', 'Teste', 'SC');
INSERT INTO endereco (id_endereco, logradouro, numero, cep, bairro, cidade, uf) VALUES (4, 'Hilda leite martins', 22, 56823688, 'RoÃ§ado ', 'SÃ£o JosÃ© ', 'Sc');
INSERT INTO endereco (id_endereco, logradouro, numero, cep, bairro, cidade, uf) VALUES (5, 'Av. Rio Grande, 1351', 1351, 88131600, 'Bairro', 'PalhoÃ§a', 'SC');
INSERT INTO endereco (id_endereco, logradouro, numero, cep, bairro, cidade, uf) VALUES (6, 'Av. Rio Grande, 1351', 1351, 88131600, 'Rio Grande', 'PalhoÃ§a', 'sc');


DROP TABLE IF EXISTS cliente;

CREATE TABLE cliente
(
  id_cliente bigint(20) NOT NULL AUTO_INCREMENT,
  cpf int(11) DEFAULT NULL,
  nome varchar(255) NOT NULL,
  email varchar(255) NOT NULL,
  id_endereco bigint(20) NOT NULL,
  PRIMARY KEY (id_cliente),
  KEY `fk_cliente_endereco` (`id_endereco`),
  CONSTRAINT `fk_cliente_endereco` FOREIGN KEY (`id_endereco`) REFERENCES `endereco` (`id_endereco`)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO cliente (id_cliente, cpf, nome, email, id_endereco) VALUES (1, 111111111, 'Mateus Alfredo Batista', 'matt.batista@hotmail.com', 1);
INSERT INTO cliente (id_cliente, cpf, nome, email, id_endereco) VALUES (2, 111111111, 'Teste', 'teste@teste.com.br', 2);
INSERT INTO cliente (id_cliente, cpf, nome, email, id_endereco) VALUES (3, 222222222, 'Teste 2', 'teste@teste.com.br', 3);
INSERT INTO cliente (id_cliente, cpf, nome, email, id_endereco) VALUES (4, 333333333, 'Jessica araujo', 'jyu.araujo@hotmail.com', 4);
INSERT INTO cliente (id_cliente, cpf, nome, email, id_endereco) VALUES (5, 0, 'Mateus Alfredo Batista', 'mateus.alfredo.batista@gmail.com', 5);
INSERT INTO cliente (id_cliente, cpf, nome, email, id_endereco) VALUES (6, 0, 'Teste', 'mateus.alfredo.batista@gmail.com', 6);

DROP TABLE IF EXISTS composicao;

CREATE TABLE composicao
(
  id_composicao bigint(20) NOT NULL AUTO_INCREMENT,
  observacao varchar(255),
  descricao varchar(255) NOT NULL,
  PRIMARY KEY (id_composicao)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO composicao (id_composicao, observacao, descricao) VALUES (1, NULL, 'Composição Hamburger');
INSERT INTO composicao (id_composicao, observacao, descricao) VALUES (2, NULL, 'Composição Misto-Quente');
INSERT INTO composicao (id_composicao, observacao, descricao) VALUES (3, NULL, 'Batata-frita');

DROP TABLE IF EXISTS forma_pagamento;

CREATE TABLE forma_pagamento
(
  id_forma_pagamento bigint(20) NOT NULL AUTO_INCREMENT,
  descricao varchar(255) NOT NULL,
  aceita_troco bit(1) NOT NULL,
  PRIMARY KEY (id_forma_pagamento)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS pedido;

CREATE TABLE pedido
(
  id_pedido bigint(20) NOT NULL AUTO_INCREMENT,
  entregue bit(1) NOT NULL,
  observacao varchar(255),
  id_endereco bigint(20) NOT NULL,
  id_cliente bigint(20) NOT NULL,
  cancelado bit(1) NOT NULL,
  PRIMARY KEY (id_pedido),
  KEY `fk_pedido_endereco` (`id_endereco`),
  KEY `fk_pedido_cliente` (`id_cliente`),
  CONSTRAINT `fk_pedido_endereco` FOREIGN KEY (`id_endereco`) REFERENCES `endereco` (`id_endereco`),
  CONSTRAINT `fk_pedido_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO pedido (id_pedido, entregue, observacao, id_endereco, id_cliente, cancelado) VALUES (1, false, NULL, 1, 1, false);
INSERT INTO pedido (id_pedido, entregue, observacao, id_endereco, id_cliente, cancelado) VALUES (2, false, NULL, 4, 4, false);
INSERT INTO pedido (id_pedido, entregue, observacao, id_endereco, id_cliente, cancelado) VALUES (3, false, NULL, 2, 2, false);

DROP TABLE IF EXISTS produto;

CREATE TABLE produto
(
  id_produto bigint(20) NOT NULL AUTO_INCREMENT,
  descricao varchar(255) NOT NULL,
  preco_custo double NOT NULL,
  preco_venda double NOT NULL,
  estoque int(11) DEFAULT NULL,
  id_composicao bigint(20),
  vendivel bit(1) NOT NULL,
  descricao_completa longtext DEFAULT NULL,
  imagem longtext DEFAULT NULL,
  PRIMARY KEY (id_produto),
  KEY `fk_produto_composicao` (`id_composicao`),
  CONSTRAINT `fk_produto_composicao` FOREIGN KEY (`id_composicao`) REFERENCES `composicao` (`id_composicao`)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO produto (id_produto, descricao, preco_custo, preco_venda, estoque, id_composicao, vendivel, descricao_completa, imagem) VALUES (1, 'Pão', 1, 2, 100, NULL, false, NULL, NULL);
INSERT INTO produto (id_produto, descricao, preco_custo, preco_venda, estoque, id_composicao, vendivel, descricao_completa, imagem) VALUES (2, 'Queijo', 1, 2, 100, NULL, false, NULL, NULL);
INSERT INTO produto (id_produto, descricao, preco_custo, preco_venda, estoque, id_composicao, vendivel, descricao_completa, imagem) VALUES (3, 'Presunto', 1, 2, 100, NULL, false, NULL, NULL);
INSERT INTO produto (id_produto, descricao, preco_custo, preco_venda, estoque, id_composicao, vendivel, descricao_completa, imagem) VALUES (4, 'Batata', 2, 4, 100, NULL, false, NULL, NULL);
INSERT INTO produto (id_produto, descricao, preco_custo, preco_venda, estoque, id_composicao, vendivel, descricao_completa, imagem) VALUES (5, 'Óleo', 1.5, 2.5, 100, NULL, false, NULL, NULL);
INSERT INTO produto (id_produto, descricao, preco_custo, preco_venda, estoque, id_composicao, vendivel, descricao_completa, imagem) VALUES (8, 'Hamburguer', 1.5, 2.5, 100, NULL, false, NULL, NULL);
INSERT INTO produto (id_produto, descricao, preco_custo, preco_venda, estoque, id_composicao, vendivel, descricao_completa, imagem) VALUES (12, 'Batata-frita', 2.5, 5, 100, NULL, true, 'Nossa batata-frita, sequinha e crocante!', 'batata-frita');
INSERT INTO produto (id_produto, descricao, preco_custo, preco_venda, estoque, id_composicao, vendivel, descricao_completa, imagem) VALUES (7, 'Água', 1.5, 2.5, 100, NULL, true, 'Água mineral, embalagem de 500ml.', 'agua');
INSERT INTO produto (id_produto, descricao, preco_custo, preco_venda, estoque, id_composicao, vendivel, descricao_completa, imagem) VALUES (11, 'Misto-Quente', 7, 12.5, 100, 2, true, 'Misto-Quente: Pão, queijo e presunto, levemente tostado, vai muito bem com um Nuke-Orange', 'misto-quente');
INSERT INTO produto (id_produto, descricao, preco_custo, preco_venda, estoque, id_composicao, vendivel, descricao_completa, imagem) VALUES (10, 'X-Burger', 10, 15, 100, 1, true, 'Nosso X-Burger feito com carne selecionada e muito bem temperada, nada de hamburger magrinho e sem gosto, aqui você come o melhor x-burger tradicional da região!', 'x-burger');
INSERT INTO produto (id_produto, descricao, preco_custo, preco_venda, estoque, id_composicao, vendivel, descricao_completa, imagem) VALUES (9, 'Nuke-Orange', 2.5, 3.5, 100, NULL, true, 'Não se deixe enganar pelo nome esquisito, o Nuke-Orange é uma delicia!', 'nuke-orange');
INSERT INTO produto (id_produto, descricao, preco_custo, preco_venda, estoque, id_composicao, vendivel, descricao_completa, imagem) VALUES (6, 'Nuke-cola', 2.5, 5, 100, NULL, true, 'Nuke-cola: O puro sabor da cola e da generosa quantia de 28 colheres de açucar!', 'nuke-cola');

DROP TABLE IF EXISTS usuario;

CREATE TABLE usuario
(
  id_usuario bigint(20) NOT NULL AUTO_INCREMENT,
  login varchar(255) NOT NULL,
  senha varchar(255) NOT NULL,
  id_cliente bigint(20) NOT NULL,
  grupo int(11) DEFAULT NULL,
  PRIMARY KEY (id_usuario),
  KEY `fk_usuario_cliente` (`id_cliente`),
  CONSTRAINT `fk_usuario_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`)
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO usuario (id_usuario, login, senha, id_cliente, grupo) VALUES (2, 'teste', '1234', 2, 1);
INSERT INTO usuario (id_usuario, login, senha, id_cliente, grupo) VALUES (3, 'testej', '1234', 1, 1);
INSERT INTO usuario (id_usuario, login, senha, id_cliente, grupo) VALUES (4, 'Teste2', '1234', 3, 1);
INSERT INTO usuario (id_usuario, login, senha, id_cliente, grupo) VALUES (6, 'teste5', '1234', 5, 1);
INSERT INTO usuario (id_usuario, login, senha, id_cliente, grupo) VALUES (7, 'teste6', '1234', 6, 1);
INSERT INTO usuario (id_usuario, login, senha, id_cliente, grupo) VALUES (1, 'mateus', '1234', 1, 99);
INSERT INTO usuario (id_usuario, login, senha, id_cliente, grupo) VALUES (5, 'Je', '1234', 4, 99);

CREATE TABLE venda
(
  id_venda bigint(20) NOT NULL AUTO_INCREMENT,
  data date NOT NULL,
  valor_pago double NOT NULL,
  valor_troco double NOT NULL,
  observacao varchar(255) NOT NULL,
  id_pedido bigint(20) NOT NULL,
  id_forma_pagamento bigint(20) NOT NULL,
  PRIMARY KEY (id_venda),
  KEY `fk_venda_pedido` (`id_pedido`),
  KEY `fk_venda_forma_pagamento` (`id_forma_pagamento`),
  CONSTRAINT `fk_venda_pedido` FOREIGN KEY (`id_pedido`) REFERENCES `pedido` (`id_pedido`),
  CONSTRAINT `fk_venda_forma_pagamento` FOREIGN KEY (`id_forma_pagamento`) REFERENCES `forma_pagamento` (`id_forma_pagamento`)
  
)ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE rel_pedido_produtos (
    id_pedido bigint(20) NOT NULL,
    id_produto bigint(20) NOT NULL,
    KEY `fk_rel_pedido_produto_pedido` (`id_pedido`),
    KEY `fk_rel_pedido_produto_produto` (`id_produto`),
    CONSTRAINT `fk_rel_pedido_produto_pedido` FOREIGN KEY (`id_pedido`) REFERENCES `pedido` (`id_pedido`),
    CONSTRAINT `fk_rel_pedido_produto_produto` FOREIGN KEY (`id_produto`) REFERENCES `produto` (`id_produto`)
);

INSERT INTO rel_pedido_produtos (id_pedido, id_produto) VALUES (1, 12);
INSERT INTO rel_pedido_produtos (id_pedido, id_produto) VALUES (2, 12);
INSERT INTO rel_pedido_produtos (id_pedido, id_produto) VALUES (1, 9);
INSERT INTO rel_pedido_produtos (id_pedido, id_produto) VALUES (1, 10);
INSERT INTO rel_pedido_produtos (id_pedido, id_produto) VALUES (3, 12);
INSERT INTO rel_pedido_produtos (id_pedido, id_produto) VALUES (3, 11);
INSERT INTO rel_pedido_produtos (id_pedido, id_produto) VALUES (1, 6);
INSERT INTO rel_pedido_produtos (id_pedido, id_produto) VALUES (1, 7);
INSERT INTO rel_pedido_produtos (id_pedido, id_produto) VALUES (1, 11);

CREATE TABLE rel_produto_composicao (
	id bigint(20) NOT NULL,
    id_produto bigint(20) NOT NULL,
    id_composicao bigint(20) NOT NULL,
    KEY `fk_rel_produto_composicao_produto` (`id_produto`),
    KEY `fk_rel_produto_composicao_composicao` (`id_composicao`),
    CONSTRAINT `fk_rel_produto_composicao_composicao` FOREIGN KEY (`id_composicao`) REFERENCES `composicao` (`id_composicao`),
    CONSTRAINT `fk_rel_produto_composicao_produto` FOREIGN KEY (`id_produto`) REFERENCES `produto` (`id_produto`)
);

INSERT INTO rel_produto_composicao (id, id_produto, id_composicao) VALUES (1, 1, 1);
INSERT INTO rel_produto_composicao (id, id_produto, id_composicao) VALUES (2, 2, 1);
INSERT INTO rel_produto_composicao (id, id_produto, id_composicao) VALUES (3, 3, 1);
INSERT INTO rel_produto_composicao (id, id_produto, id_composicao) VALUES (4, 8, 1);
INSERT INTO rel_produto_composicao (id, id_produto, id_composicao) VALUES (5, 1, 2);
INSERT INTO rel_produto_composicao (id, id_produto, id_composicao) VALUES (6, 2, 2);
INSERT INTO rel_produto_composicao (id, id_produto, id_composicao) VALUES (7, 3, 2);
INSERT INTO rel_produto_composicao (id, id_produto, id_composicao) VALUES (8, 4, 3);
INSERT INTO rel_produto_composicao (id, id_produto, id_composicao) VALUES (9, 5, 3);


